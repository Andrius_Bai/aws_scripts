from mock import Mock
from scripts.change_acc_status import change_acc_status


def test_acc_status(mocker):
    mocked_parser = Mock()
    mocked_password = Mock()
    mocked_user_pool_id = Mock()
    mocked_subprocess = Mock()

    mocked_parser.return_value = 'my_pool_id'
    mocked_user_pool_id.return_value = 'accc'
    mocked_password.return_value = '123'
    mocked_subprocess.return_value = 'done'

    mocker.patch('scripts.change_acc_status.parser', mocked_parser)
    mocker.patch('scripts.change_acc_status.USER_POOL_ID', mocked_user_pool_id)
    mocker.patch('scripts.change_acc_status.PASSWORD', mocked_password)
    mocker.patch('scripts.change_acc_status.subprocess.run', mocked_subprocess)
    actual_result = change_acc_status('testas')
    expected_result = 'done'
    assert actual_result == expected_result
    mocked_subprocess.assert_called_with('brrrr')
