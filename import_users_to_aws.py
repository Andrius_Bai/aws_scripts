import subprocess
import sys
import json
import time

try:
    import_job_name = sys.argv[1]
    user_pool_id = sys.argv[2]
    cognito_pool_region = 'eu-central-1'

    # create user import job
    cloud_watch_logs_role_arn = 'arn:aws:iam::982630452742:role/service-role/Cognito-UserImport-Role'
    form_cmd = f"aws cognito-idp create-user-import-job --job-name {import_job_name} " \
               f"--user-pool-id {user_pool_id} " \
               f"--cloud-watch-logs-role-arn {cloud_watch_logs_role_arn}"
    cmd_run = form_cmd.split(' ')
    result = subprocess.run(cmd_run, capture_output=True)
    # import pdb; pdb.set_trace()  # line for subbprocess output debug
    result_json = json.loads(result.stdout)
    pre_signed_url = result_json["UserImportJob"]["PreSignedUrl"]
    import_job_id = result_json["UserImportJob"]["JobId"]

    print(pre_signed_url)
    print("--------------------------------------")
    print("User Import job created")

    # uploading the .csv file

    path_to_csv = 'aws_template.csv'
    form_cmd_import = f"curl -v -T {path_to_csv} " \
                      f"-H x-amz-server-side-encryption:aws:kms {pre_signed_url}"
    cmd_client = form_cmd_import.split(' ')
    cmd_client_result = str(subprocess.run(cmd_client, capture_output=True))
    print(cmd_client_result)
    print(".csv import finished")
    print("--------------------------------------")

    # start User imort job
    # sleep period before start the import
    # time.sleep(5)
    #
    # form_cmd_start = f"aws cognito-idp start-user-import-job --user-pool-id {user_pool_id} --job-id {import_job_id}"
    # cmd_start = form_cmd_import.split(' ')
    # cmd_start_result = str(subprocess.run(cmd_client, capture_output=True))
    # print(cmd_start_result)
    # print(f"User import job {import_job_name} started.")


except IndexError:
    print("Please enter <import job-name> <User_Pool_id>")

