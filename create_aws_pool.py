import subprocess
import sys
import json
import urllib.request

region = 'eu-central-1'

try:
    pool_name = sys.argv[1]
    pool_client_name = sys.argv[2]
    form_cmd = f"aws cognito-idp create-user-pool --pool-name {pool_name}-pool --username-attributes email " \
               f"--output json"
    cmd_run = form_cmd.split(' ')
    result = subprocess.run(cmd_run, capture_output=True)
    result_json = json.loads(result.stdout)

    userPoolId = result_json["UserPool"]["Id"]

    print("User_Pool_Id: " + userPoolId)
    print("--------------------------------------")

    form_cmd_client = f"aws cognito-idp create-user-pool-client --user-pool-id {userPoolId} " \
                      f"--client-name {pool_client_name}-app-pool --no-generate-secret"
    cmd_client = form_cmd_client.split(' ')
    print("Pool User Client creation process:")
    cmd_client_result = subprocess.run(cmd_client, capture_output=True)
    cmd_client_result_json = json.loads(cmd_client_result.stdout)
    client_cognito_id = cmd_client_result_json["UserPoolClient"]["ClientId"]

    # import pdb; pdb.set_trace()  # uncheck line for subbprocess output debug

    print("------------------------------")
    # read the data from the URL and print it
    # open a connection to a URL using urllib
    webUrl = urllib.request.urlopen(f'https://cognito-idp.{region}.amazonaws.com/{userPoolId}/.well-known/jwks.json')

    # get the result code and print it
    print("JWKS url response code: " + str(webUrl.getcode()))

    # read the data from the URL and print it
    data = json.loads(webUrl.read().decode(webUrl.info().get_param('charset') or 'utf-8'))
    # jwks_for_env = json.dumps(data)
    print("COGNITO_POOL_ID: str = " + "'" + userPoolId + "'")
    print("COGNITO_CLIENT_ID: str = " + "'" + client_cognito_id + "'")
    print("COGNITO_POOL_REGION: str = " + "'" + region + "'")
    print("COGNITO_JWKS: str = " + json.dumps(data, indent=4))

except IndexError:
    print("Please enter <User_Pool_name> <User_Pool_client_name>")
