# README #

This README would document whatever steps are necessary to complete at AWS level to get your recar.lt up and running locally on your machine.

### What is this repository for? ###

* To create AWS User Pool, User Pool Client, import Users and change imported Users account status too CONFIRMED.
* repository is needed to complete https://recaras.atlassian.net/wiki/spaces/RECAR/pages/354910241/Create+AWS+User+pool+from+CLI
* repository is needed to complete https://recaras.atlassian.net/wiki/spaces/RECAR/pages/352059393/Running+Recar+locally

### How do I get set up? ###

* Create virtualenvironment at repository
* Activate virtualenvironment
* pip install -r requirements.txt

### Who do I talk to? ###

* User who need to run recar.lt locally
* User who need to import batch of new users to AWS User Pool Client from CLI
* User who need to change User Pool client user account status

### How to cognito users

# -attr values can be found in cognito -> user-pool -> Users and Groups -> click on any user id

* python export_aws_users.py -attr sub email --user-pool-id <your_pool_id>