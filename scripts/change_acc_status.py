import argparse
import subprocess
import csv

REGION = ''
USER_POOL_ID = ''
CSV_FILE_NAME = 'updateDB/CognitoUsers.csv'
PASSWOORD = ''

""" Parse All Provided Arguments """
parser = argparse.ArgumentParser(description='Cognito User Pool change Account Status to CONFIRMED')
parser.add_argument('--user-pool-id', type=str, help="The user pool ID", required=True)
parser.add_argument('--region', type=str, default='eu-central-1', help="The user pool region")
parser.add_argument('--password', type=str, default='bbzBBZ0.',
                    help="New User Password. Need to change on first login.")
parser.add_argument('-f', '--file-name', type=str, help="CSV File name")
args = parser.parse_args()

if args.user_pool_id:
    USER_POOL_ID = args.user_pool_id
if args.region:
    REGION = args.region
if args.file_name:
    CSV_FILE_NAME = args.file_name
if args.password:
    PASSWORD = args.password


def change_acc_status(username):
    form_cmd = f"aws cognito-idp admin-set-user-password " \
               f"--user-pool-id {USER_POOL_ID} " \
               f"--username {username} --password {PASSWORD} --permanent"
    cmd_run = form_cmd.split(' ')
    result = subprocess.run(cmd_run, capture_output=True)

    return result


def main():
    with open(CSV_FILE_NAME, mode='r') as aws_usernames:
        csv_reader = csv.reader(aws_usernames)
        for row in csv_reader:
            username = row[1]
            output = change_acc_status(username)
            print(output)


if __name__ == '__main__':
    main()



